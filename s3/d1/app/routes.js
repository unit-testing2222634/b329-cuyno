const { names } = require('../src/util');

module.exports = app => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })

    app.post('/person', (req, res) => {
        if (!req.body.hasOwnProperty('name')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }

        if (typeof req.body.name !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }

        if (!req.body.hasOwnProperty('alias')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            })
        }
        
        if (typeof req.body.alias !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request - ALIAS has to be a string'
            })
        }

        if (!req.body.hasOwnProperty('age')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        
        if (typeof req.body.age !== 'number') {
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })	
        }

        return res.sendStatus(200)
    })

    app.post('/users', (req, res) => {
        if (!req.body.hasOwnProperty('username')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter USERNAME'
            })
        }

        if (typeof req.body.username !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request - USERNAME has to be a string'
            })
        }

        if (!req.body.hasOwnProperty('age')) {
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        
        if (typeof req.body.age !== 'number') {
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })	
        }

        return res.sendStatus(200)
    })
}