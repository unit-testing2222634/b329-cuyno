const chai = require('chai')
const chai_http = require('chai-http')
const { expect } = chai
chai.use(chai_http)

// S3 Activity
describe('api_test_suite', () => {
    it('test_api_people_is_running', () => {
        chai.request('http://localhost:5001').get('/people').end((err, res) => {
            expect(res).to.not.equal(undefined)
        })
    })

    it('test_api_people_returns_200', done => {
        chai.request('http://localhost:5001').get('/people').end((err, res) => {
            expect(res.status).to.equal(200)
            done()
        })
    })

    it('test_api_post_person_returns_400_if_no_person_name', done => {
        chai.request('http://localhost:5001').post('/person').type('json')
            .send({ 
                alias: 'Jason',
                age: 28
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

    it('test_api_post_person_is_running', done => {
        chai.request('http://localhost:5001').post('/person').type('json')
            .send({ 
                name: 'Jason',
                age: 28,
                alias: 'Jayson'
            })
            .end((err, res) => {
                expect(res.status).to.equal(200)
                done()
            })
    })

    it('test_api_post_person_returns_400_if_no_alias', done => {
        chai.request('http://localhost:5001').post('/person').type('json')
            .send({ 
                name: 'Jason',
                age: 28
            })
            .end(async (err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

    it('test_api_post_person_returns_400_if_no_age', done => {
        chai.request('http://localhost:5001').post('/person').type('json')
            .send({ 
                name: 'Jason',
                alias: 'Jayson'
            })
            .end(async (err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })
})