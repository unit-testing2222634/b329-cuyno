function factorial(n) {
    // if (typeof n !== 'number') return undefined;
    // if (n < 0) return undefined;
    
    if (n === 0) 
        return 1
    if (n === 1) 
        return 1
    return n * factorial(n - 1)
}

function check_if_divisible_by_5_or_7(n) {
    return n % 5 == 0 || n % 7 == 0 
}

const names = {
    'Brandon': {
        'name': 'Brandon Boyd',
        'age': 35
    },
    'Steve': {
        'name': 'Steve Tyler',
        'age': 56
    }
}

module.exports = { 
    factorial, 
    check_if_divisible_by_5_or_7, 
    names 
}