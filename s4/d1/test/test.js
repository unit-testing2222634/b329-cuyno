const { factorial, check_if_divisible_by_5_or_7 } = require('../src/util')
const chai = require('chai')
const chai_http = require('chai-http')
const { expect, assert } = chai
chai.use(chai_http)

describe('test_fun_factorials', () => {
    it('test_fun_factorial_5!_is_120', () => {
        const product = factorial(5)
        expect(product).to.equal(120)
    })
    
    it('test_fun_factorial_1!_is_1', () => {
        const product = factorial(1)
        assert.equal(product, 1)
    })

    it('test_fun_factorial_0!_is_1', () => {
        const product = factorial(1)
        assert.equal(product, 1)
    })

    it('test_fun_factorial_4!_is_24', () => {
        const product = factorial(4)
        assert.equal(product, 24)
    })

    it('test_fun_factorial_10!_is_3628800', () => {
        const product = factorial(10)
        assert.equal(product, 3628800)
    })

    it('test_fun_factorial_neg1_is_undefined', () => {
        const product = factorial(-1)
        expect(product).to.equal(undefined)
    })

    it('test_fun_factorial_invalid_number_is_undefined', () => {
        const product = factorial('Johnny')
        expect(product).to.equal(undefined)
    })
})

describe('test_divisibilty_by_5_or_7', () => {
    it('test_100_is_divisible_by_5_or_7', () => {
        const divisibility = check_if_divisible_by_5_or_7(100)
        assert.equal(divisibility, true)
    })
    
    it('test_49_is_divisible_by_5_or_7', () => {
        const divisibility = check_if_divisible_by_5_or_7(49)
        assert.equal(divisibility, true)
    })

    it('test_30_is_divisible_by_5_or_7', () => {
        const divisibility = check_if_divisible_by_5_or_7(30)
        assert.equal(divisibility, true)
    })

    it('test_56_is_divisible_by_5_or_7', () => {
        const divisibility = check_if_divisible_by_5_or_7(56)
        assert.equal(divisibility, true)
    })
})

// S3 Activity
describe('api_test_suite_users', () => {
    it('test_api_users_is_running', () => {
        chai.request('http://localhost:5001').post('/users').type('json')
            .send({ 
                username: 'Jason',
                age: 21,
            })
            .end((err, res) => {
                expect(res.status).to.equal(200)
            })
    })

    it('test_api_post_users_returns_400_if_no_age', () => {
        chai.request('http://localhost:5001').post('/users').type('json')
            .send({ 
                username: 'Jason',
                name: 'Jason',
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
            })
    })

    it('test_api_post_users_returns_400_if_no_username', () => {
        chai.request('http://localhost:5001').post('/users').type('json')
            .send({ 
                name: 'Jason',
                age: 28
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
            })
    })
})