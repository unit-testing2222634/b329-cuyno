function factorial(n){
    // GREEN - Write code to pass the test
    if(typeof n !== 'number') return undefined; 
    // GREEN - Write code to pass the test
    if(n<0) return undefined; 
    if(n===0) return 1;
    if(n===1) return 1;
    return n * factorial(n-1);
    // 4 * 3 * 2 * 1
}

// In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
// If the number received is divisible by 5, return true.
// If the number received is divisible by 7, return true.
// Return false if otherwise
function div_check(n){

    if(n%5 === 0) return true
    if(n%7 === 0) return true
    return false

}

const names = {
    "Brandon": {
        "name": "Brandon Boyd",
        "age": 35
    },
    "Steve": {
        "name": "Steve Tyler",
        "age": 56
    }
}

// S4 Activity Template START
const users = [
    {
        username: "brBoyd87",
        password: "87brandon19"

    },
    {
        username: "tylerofsteve",
        password: "stevenstyle75"
    }
]
// S4 Activity Template END

module.exports = {
    factorial: factorial,
    div_check: div_check,
    names: names,
    users: users
}