const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		const { name, ex, alias } = req.body

		if (!(typeof name == 'string' && name)) 
            return res.sendStatus(400)
		if (!(typeof ex == 'object' && ex)) 
            return res.sendStatus(400)
		if (!(typeof alias == 'string' && alias)) 
            return res.sendStatus(400)

		const has_duplicate = Object.values(exchangeRates).filter(exchangeRate => exchangeRate.name == alias).length
		return res.sendStatus(has_duplicate ? 400 : 200)
	})
}
