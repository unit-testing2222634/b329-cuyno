const chai = require('chai')
const expect = chai.expect;
const http = require('chai-http')
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})	
})

describe('forex_api_test_suite_currency', () => {
    it('test_api_post_currencies_is_running', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
                name: 'United States Dollar',
				ex: {
					'peso': 50.73,
					'won': 1187.24,
					'yen': 108.63,
					'yuan': 7.03
				},
				alias: 'Mexican Peso'
            })
            .end((err, res) => {
                expect(res.status).to.equal(200)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_name_is_missing', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
                currency: 'United States Dollar'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_name_is_not_a_string', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 1
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_name_is_empty', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: ''
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_ex_is_missing', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_ex_is_not_an_object', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar',
				ex: 1234.34
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_ex_is_empty', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar',
				ex: ''
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_alias_is_missing', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar',
				ex: {
					'peso': 50.73,
					'won': 1187.24,
					'yen': 108.63,
					'yuan': 7.03
				}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_alias_is_not_an_string', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar',
				ex: {
					'peso': 50.73,
					'won': 1187.24,
					'yen': 108.63,
					'yuan': 7.03
				},
				alias: true
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_alias_is_empty', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar',
				ex: {
					'peso': 50.73,
					'won': 1187.24,
					'yen': 108.63,
					'yuan': 7.03
				},
				alias: ''
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_returns_status_400_if_all_fields_are_complete_but_there_is_a_duplicate_alias', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar',
				ex: {
					'peso': 50.73,
					'won': 1187.24,
					'yen': 108.63,
					'yuan': 7.03
				},
				alias: 'United States Dollar'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400)
                done()
            })
    })

	it('test_api_post_currencies_200_if_all_fields_are_complete_and_there_are_no_duplicates', done => {
        chai.request('http://localhost:5001').post('/currency').type('json')
            .send({ 
				name: 'United States Dollar',
				ex: {
					'peso': 50.73,
					'won': 1187.24,
					'yen': 108.63,
					'yuan': 7.03
				},
				alias: 'Mexican Peso'
            })
            .end((err, res) => {
                expect(res.status).to.equal(200)
                done()
            })
    })
})